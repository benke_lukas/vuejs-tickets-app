# VueJS app for price tags

This desktop application is for managing price tags. You can define name, EAN code, price, unit and conversion to price per unit.

The application can import properly formatted CSV from various accounting systems.

The application can then print price tags with all information

## Technologies
- Vue.js
- Electron
- CouchDB
- Bootstrap