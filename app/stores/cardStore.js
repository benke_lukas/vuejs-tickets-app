/**
 * Created by ben on 26.8.16.
 */
export default {
  cards: [],

  conversions: {
    'g' : 'kg',
    'ml' : 'l',
    'l'  : 'l'
  }
}